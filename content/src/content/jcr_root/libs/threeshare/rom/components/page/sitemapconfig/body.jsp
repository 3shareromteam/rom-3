<%@include file="/libs/foundation/global.jsp" %>

<%--
  ~ Copyright 2014 3|SHARE Corporation
  ~ All Rights Reserved.
--%>

<body>
	<div class="centerContainer">
	    <h1>Sitemap Configuration</h1>

    	<cq:include path="sitemapconfig" resourceType="/libs/threeshare/rom/components/page/sitemapconfig/generalconfig"/>

    </div>
</body>