<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>

<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="com.threeshare.rom.search.SearchUtil" %>
<%@ page import="com.threeshare.rom.util.RequestUtils" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.sling.api.resource.Resource" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="javax.jcr.*"%>


<%

    Session session = resourceResolver.adaptTo(Session.class);
	String defaultSearchPath = "/content/dam";
	String searchPath = RequestUtils.getParameter(request,"searchPath",defaultSearchPath);
	String defaultAssetType = "dam:Asset";
	String assetType = RequestUtils.getParameter(request,"assetType",defaultAssetType);
	String defaultTagDelimiter = "<br>";
	String tagDelimiter = RequestUtils.getParameter(request,"tagDelimiter",defaultTagDelimiter);
	String[] metadataFields = RequestUtils.getParameterArray(request,"metadataFields");
	String defaultMetadataPath = "";
	String metadataPath = RequestUtils.getParameter(request,"metadataPath",defaultMetadataPath);

	List<Resource> searchResources = SearchUtil.searchByType(searchPath, resourceResolver, assetType);

%>
    <style>
      tr:nth-child(even) {background-color: #f2f2f2}
      table { width: 100%; border-collapse: collapse; }
      th { height: 50px; }
      table, th, td { border: 1px solid black; }
      th { height: 30px; }
    </style>

    <table>
    <tr>
        <th>Path</th>
        <th>Name</th>
        <th>Last Modified Date</th>
        <th>Tags</th>
<%

    if (metadataFields != null)
	{
        for (String metadataField : metadataFields)
        {
            if (metadataField != null && metadataField.length() > 0)
            {
    %>        <th><%=metadataField%></th>
    <%

            }
        }
	}


%>

    </tr>
<%
    for (Resource searchResource: searchResources)
    {
		    if (searchResource != null)
        {
            Node node = searchResource.adaptTo(Node.class); // com.day.cq.security.User
            if (node != null)
            {
				String lastModified = "";
                StringBuilder sbTags = new StringBuilder();
				String nodePath = node.getPath();
                if (node.hasProperty("jcr:content/jcr:lastModified"))
	            {
    	        	Calendar cal = node.getProperty("jcr:content/jcr:lastModified").getValue().getDate();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                    lastModified = dateFormat.format(cal.getTime());
                }
                String name = node.getName();
                String pdfTitle = "";
				if (node.hasProperty("jcr:content/metadata/pdf:Title"))
	            {
                    pdfTitle = node.getProperty("jcr:content/metadata/pdf:Title").getValue().getString();
                }
                Resource damResource = resourceResolver.getResource(searchResource.getPath() + "/jcr:content/metadata");
    			if (damResource != null)
    			{
			        ValueMap damProperties = damResource.adaptTo(ValueMap.class);
			        String[] damTags = damProperties.get("cq:tags", String[].class);

			        if (damTags != null)
        			{
                    	for (String damTag : damTags)
                		{
					    	if (damTag != null && damTag.length() > 0)
					        {
								sbTags.append(damTag + tagDelimiter);
                            }
                        }

                    }
                }
                String tags = sbTags.toString();

                %>
            		<tr>
    			      <th><%=nodePath%></th>
    			      <th><%=name%></th>
    			      <th><%=lastModified%></th>
    			      <th><%=tags%></th>
<%

    if (metadataFields != null)
	{
        for (String metadataField : metadataFields)
        {
            String metadataFieldValue="";
            if (metadataField != null && metadataField.length() > 0)
            {
                String relativePropertyPath = metadataPath + "/" + metadataField;

             	if (node.hasProperty(relativePropertyPath))
	            {
					int propertyType = node.getProperty(relativePropertyPath).getType();
                    boolean isMulti = node.getProperty(relativePropertyPath).getDefinition().isMultiple();

					if (propertyType == PropertyType.STRING)
                    {
                        //PropertyType.STRING
                        if (isMulti)
                        {

                            Value[] multiMetadataFieldValue = node.getProperty(relativePropertyPath).getValues();
							StringBuilder strBuilder = new StringBuilder();
                            for (int i = 0; i < multiMetadataFieldValue.length; i++)
                            {
                                strBuilder.append(multiMetadataFieldValue[i].getString());
                            }
							metadataFieldValue = strBuilder.toString();

						}
                        else
                        {
							metadataFieldValue = node.getProperty(relativePropertyPath).getValue().getString();
                        }
                    }
                    else if (propertyType == PropertyType.BINARY)
                    {
                        //PropertyType.BINARY
                        if (isMulti)
                        {

                        }
                        else
                        {

                        }
                    }
                    else if (propertyType == PropertyType.DATE)
                    {
                        //PropertyType.DATE
                        if (isMulti)
                        {

                        }
                        else
                        {
		    	        	Calendar cal = node.getProperty(relativePropertyPath).getValue().getDate();
		                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        		            metadataFieldValue = dateFormat.format(cal.getTime());
                        }
                    }
                    else if (propertyType == PropertyType.DOUBLE)
                    {
                        //PropertyType.DOUBLE
                        if (isMulti)
                        {

                        }
                        else
                        {
                            double doubleMetadataFieldValue = node.getProperty(relativePropertyPath).getDouble();
                            metadataFieldValue = String.valueOf(doubleMetadataFieldValue);
                        }
                    }
                    else if (propertyType == PropertyType.LONG)
                    {
                        //PropertyType.LONG
                        if (isMulti)
                        {

                        }
                        else
                        {
							Long longMetadataFieldValue = node.getProperty(relativePropertyPath).getLength();
                            metadataFieldValue = String.valueOf(longMetadataFieldValue);
                        }
                    }
                    else if (propertyType == PropertyType.BOOLEAN)
                    {
                        //PropertyType.BOOLEAN
                        if (isMulti)
                        {

                        }
                        else
                        {

                        }
                    }
                    else if (propertyType == PropertyType.NAME)
                    {
                        //PropertyType.NAME
                        if (isMulti)
                        {

                        }
                        else
                        {

                        }
                    }
                    else if (propertyType == PropertyType.PATH)
                    {
                        //PropertyType.PATH
                        if (isMulti)
                        {

                        }
                        else
                        {

                        }
                    }
                    else if (propertyType == PropertyType.REFERENCE)
                    {
                        //PropertyType.REFERENCE
                        if (isMulti)
                        {

                        }
                        else
                        {

                        }
                    }
                    //metadataFieldValue = node.getProperty(relativePropertyPath).getValue().getString();
                }



%>        <th><%=metadataFieldValue%></th>
<%

            }
        }
	}

%>


            		</tr>
        		    <%
            }
        }
    }
%>

</table>
