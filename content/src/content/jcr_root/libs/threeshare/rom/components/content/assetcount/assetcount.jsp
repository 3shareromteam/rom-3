<%@include file="/libs/foundation/global.jsp" %><%
%><%@page import="java.util.List" %><%
%><%@page import="java.util.ArrayList" %><%
%><%@page import="org.apache.sling.api.resource.ResourceUtil" %><%
%><%@page import="javax.jcr.Node" %><%
%><%@page import="javax.jcr.NodeIterator" %><%
%><%@page import="com.threeshare.rom.util.NodeUtils" %><%    
%><%@page import="com.threeshare.rom.util.RequestUtils" %><%
%><%@page import="com.threeshare.rom.search.SearchUtil" %><%    


%><%

Session session = resourceResolver.adaptTo(Session.class);

String defaultSearchPath = "/content/dam";
String searchPath = RequestUtils.getParameter(request,"searchPath",defaultSearchPath);

String defaultAssetType = "dam:Asset";
String assetType = RequestUtils.getParameter(request,"assetType",defaultAssetType);

// maxDepth Parameter
String maxDepthParameter = "maxDepth";    
String maxDepthStr = RequestUtils.getParameter(request, maxDepthParameter);
int maxDepth = 1;
if (maxDepthStr != null && maxDepthStr.matches("^\\d+$") && Integer.valueOf(maxDepthStr) != null)
{
	maxDepth = Integer.valueOf(maxDepthStr);
}

int maxLevelIn = 1;
int maxWorkFlowToPurge = 100000;
int count = 0;

List<String> paths = NodeUtils.crawlNodes(resourceResolver,searchPath,maxWorkFlowToPurge,maxDepth);

for (String path : paths)
{
    Resource searchResource = resourceResolver.getResource(path);
    if (searchResource != null)
    {   
		Long searchResultCount = SearchUtil.assetSearchCount(path,resourceResolver,assetType);
        Node node = searchResource.adaptTo(Node.class);
        %><%=path %>,<%=searchResultCount%><br><%

    }
    else
    {
        %>Node <%=path%> Not Found.<br><br><%
    }
}
%>

