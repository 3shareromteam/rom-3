<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import="com.day.cq.search.*,
    com.day.cq.search.result.SearchResult, 
    com.day.cq.search.result.Hit,
    org.apache.sling.api.resource.ResourceUtil, 
    org.apache.sling.api.resource.ValueMap,
    java.util.HashMap,
    org.json.JSONObject, 
    java.util.List,
    java.util.ArrayList,
    java.util.Arrays,
    java.util.Map,
    com.threeshare.rom.util.ROMConstants,
    com.threeshare.rom.search.SearchUtil,
    org.apache.jackrabbit.JcrConstants,
    com.threeshare.rom.vanity.*,
    com.threeshare.rom.util.RequestUtils"%><%

    
    /*************************************************************************
     * Initializations
     *************************************************************************/
     
    String customVanityProperty = ROMConstants.VANITY_TYPE_CUSTOM_JCR_PROPERTY;
    String vanityProperty = ROMConstants.VANITY_TYPE_STANDARD_JCR_PROPERTY;
    String defaultRedirectType = ROMConstants.DEFAULT_REDIRECT_TYPE;
    String vanityURLKey = ROMConstants.VANITY_URI_PAGE_PROPERTY_KEY;
    String redirectTypeKey = ROMConstants.REDIRECT_TYPE_PAGE_PROPERTY_KEY; 
    
    
    /*************************************************************************
     * Read Query Parameters
     *************************************************************************/
     
    //Set Default Content Path
    String content = "/content";
    String contentParam = RequestUtils.getParameter(request,"content");
    content = contentParam == null ? content : contentParam;
    
    // Set Rewrite Parameters
    String rewriteParam = "";

    //Set Remove Path, used for removing content path to match Resource Resolver rules
    String removePath = "";

    //Set Exclusion Paths from comma separated exclude parameter
    String excludePathsParam = "";

    //Set Append Path, used for append content path to match Resource Resolver rules
    String defaultAppendPath = "";

    //Set Vanity type
    String vanityType = "standard";

    
    /*************************************************************************
     * Find and Output Vanities
     *************************************************************************/
%>
<table>
  <tr>
    <td>Vanity</td>
    <td>Target</td>
    <td>Redirect Type</td>
  </tr>



<%
    String configPath = currentNode.getPath() + "/config";
    VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
    List<Vanity> vanitiesList = new ArrayList<Vanity>();

    //vanitiesList.add(new Vanity("/vanity" , "/target" , "301"));

    for (Vanity vanity : vanitiesList)
    {
        String vanityURI = vanity.getVanityURI();
        String vanityTarget = vanity.getVanityTarget();
        String redirectType = vanity.getRedirectType();

        VanityNode vanityCreateNode = configNode.createVanity(vanityURI,vanityTarget,redirectType);

        if (vanityURI.length() > 0 && vanityURI != null && vanityCreateNode != null)

        {%>
              <tr>
                <td><%=vanityURI%></td>
                <td><%=vanityTarget%></td>
                <td><%=redirectType%></td>
              </tr>
        <%
        
        }
    }
    
%>
</table>