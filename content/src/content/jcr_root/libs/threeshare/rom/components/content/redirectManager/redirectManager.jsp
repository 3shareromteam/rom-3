<%--
  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>
<%@include file="/libs/foundation/global.jsp" %>
<%@ page import="com.threeshare.rom.config.ROMConfigManager" %>
<%@ page import="com.threeshare.rom.util.ROMConstants" %>
<%@ page import="com.threeshare.rom.util.RequestUtils" %>
<%@ page import="org.apache.sling.api.request.RequestPathInfo" %>
<%@ page import="com.threeshare.rom.vanity.VanityFactory"%>
<%@ page import="com.threeshare.rom.vanity.VanityConfigNode"%>
<%@ page import="com.threeshare.rom.vanity.VanityNode"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Collections"%>

<%

// Page Properties
// *************************************************************
String headerText = currentPage.getTitle() == null ? xssAPI.encodeForHTML(currentPage.getName()) : xssAPI.encodeForHTML(currentPage.getTitle());
String configPath = currentPage.getPath() + "/jcr:content/redirects";
//*************************************************************

// Default Values
// *************************************************************
String vanityDefaultText = "Vanity";
String targetDefaultText = "Target";
String vanityDescription = "When a user types in the vanity URL address, they " +
    "will be redirected to the corresponding target URL. Please configure the " +
    "vanity URL, target URL and redirect type";
String defaultRedirectType = ROMConstants.DEFAULT_REDIRECT_TYPE;
String y302 = defaultRedirectType.equals("302") ? "checked" : "";
String y301 = defaultRedirectType.equals("301") ? "checked" : "";
String NODE_ATTR = "data-node";
String vanityFormID = "formExternalVanity";
String newVanity = ROMConstants.NEW_VANITY_TAG;
String invalidConfigMessage = "Invalid Configuration Node";
String errorClass = "error";
String sortParam = "sort";
String sortDescend = "descend";
boolean allowNewVanity = true;
//*************************************************************

// Vanity Configuration
// *************************************************************
// Get Vanity configuration node
VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
boolean invalidConfig = configNode == null;
// Get Vanities List from ConfigNode
List<VanityNode> vanities = invalidConfig ? new ArrayList<VanityNode>() : configNode.getVanities();
// Retrieve sort paramter
String sort = RequestUtils.getParameter(request,sortParam);
boolean sortDesc = sort != null && sort.equals(sortDescend) ? true : false;

if (sortDesc) {
    // Sort descending based on vantityURI
    Collections.sort(vanities, VanityNode.vanityURIComparatorDesc);
} else {
    // Sort ascending based on vantityURI
    Collections.sort(vanities, VanityNode.vanityURIComparatorAsc);
}
//*************************************************************

// Set Status Message
// *************************************************************
boolean showStatusMessage = invalidConfig;
String statusMessage = "";
String statusClass = "";
if (showStatusMessage)
{
    if (invalidConfig)
    {
        statusMessage = invalidConfigMessage;
        statusClass = errorClass;
    }
}
//*************************************************************

// Begin Markup
// *************************************************************
%>


<div class="main">
    <div class="topNav"></div>
    <header>
        <div class="logo"></div>
        <h1><%=headerText %></h1>
    </header>

    <div class="container">
       <div class="formContainer">
            <form name="setExternalVanity" id="<%=vanityFormID %>" class="twcAdmin" action="<%=currentPage.getPath()%>.html" method="post">
                <div class="fields">
                    <div class="field-wrapper">
                        <div id="vanitiesContainer">
                            <div class="labelContainer"> 
                                <div class="w300 inputLabel" title="<%=vanityDescription %>">
                                    <a class="sort vanityUri" href="#">
                                     Vanity URL
                                     <span class="left sortIcon <%=sortDesc ? sortDescend : "" %>"></span>
                                    </a>
                                </div>
                                <div class="w300 inputLabel" title="<%=vanityDescription %>">Target URL</div>
                                <div class="inputLabel short" title="<%=vanityDescription %>">Redirect Type</div>
                            </div>

                            <%
                            
                            for (VanityNode vanityObj : vanities)
                            {
                                String vanity = vanityObj.getVanityURI();
                                String target = vanityObj.getVanityTarget();
                                String redirectType = vanityObj.getRedirectType().equals("") ? defaultRedirectType : vanityObj.getRedirectType();
                                String nodePath = vanityObj.getPath();
                                String x302 = redirectType.equals("302") ? "checked" : "";
                                String x301 = redirectType.equals("301") ? "checked" : "";

                                String x302cls = redirectType.equals("302") ? "activeRedirectType" : "";
                                String x301cls = redirectType.equals("301") ? "activeRedirectType" : "";
                                
                                %>
                                <div class="vanityURL">
                                    <input class="w300 vanityPath" type="text" title="Defines the vanity URL" data_default_value="<%=vanityDefaultText %>" value="<%=vanity %>" <%=NODE_ATTR %>="<%=nodePath %>"/>
                                    <input class="w300 redirectTarget" type="text" title="Defines the redirect for the vanity URL" data_default_value="<%=targetDefaultText %>" value="<%=target %>" <%=NODE_ATTR %>="<%=nodePath %>"/>
                                    <input class="checkbox <%=x301cls%>" type="checkbox" <%=x301 %> title="Selects a 301 redirect" data_value="301">
                                    <div class="inputLabel short" title="Selects a 301 redirect">301</div>
                                    <input class="checkbox <%=x302cls%>" type="checkbox" <%=x302 %> title="Selects a 302 redirect" data_value="302">
                                    <div class="inputLabel short" title="Selects a 302 redirect">302</div>
                                    <a class="removeVanity" href="#" title="Remove this vanity configuration"><span>Remove</span></a>
                                </div>
                                <%
                            }
                            
                            %>
                            
                            <%
                            if (allowNewVanity)
                            {
                                %>
                                <div class="vanityURL empty">
                                    <input class="w300 vanityPath default" type="text" data_default_value="<%=vanityDefaultText %>" value="<%=vanityDefaultText %>" <%=NODE_ATTR %>="<%=newVanity %>"/>
                                    <input class="w300 redirectTarget default" type="text" data_default_value="<%=targetDefaultText %>" value="<%=targetDefaultText %>" <%=NODE_ATTR %>="<%=newVanity %>"/>
                                    <input class="checkbox" type="checkbox" <%=y301 %> title="Selects a 301 redirect" data_value="301">
                                    <div class="inputLabel short" title="Selects a 301 redirect">301</div>
                                    <input class="checkbox" type="checkbox" <%=y302 %> title="Selects a 302 redirect" data_value="302">
                                    <div class="inputLabel short" title="Selects a 302 redirect">302</div>
                                    <a class="removeVanity" href="#" title="Remove this vanity configuration"><span>Remove</span></a>
                                </div>
                                <%
                            }
                            %> 
                        </div>
                    </div>
                </div>
            </form>          
            <div class="clear"></div>
            <div class="submit">
               <a class="form-submit" data_submit_target="<%=vanityFormID %>" href ="#">
                   <div class="submitButton">
                       Submit  
                   </div>
               </a>
            </div>
            <div class="statusMessage <%=statusClass %>">
               <div class="title"><%=statusMessage %></div>
               <div class="messageStack"></div>
            </div>
            <!-- This is used as a template to generate additional text input boxes -->
            <div class="hide template">
                <div class="vanityURL empty">
                    <input class="w300 vanityPath default" type="text" data_default_value="<%=vanityDefaultText %>" value="<%=vanityDefaultText %>" <%=NODE_ATTR %>="<%=newVanity %>"/>
                    <input class="w300 redirectTarget default" type="text" data_default_value="<%=targetDefaultText %>" value="<%=targetDefaultText %>" <%=NODE_ATTR %>="<%=newVanity %>"/>
                    <input class="checkbox" type="checkbox" <%=y301 %> title="Selects a 301 redirect" data_value="301">
                    <div class="inputLabel short" title="Selects a 301 redirect">301</div>
                    <input class="checkbox" type="checkbox" <%=y302 %> title="Selects a 302 redirect" data_value="302">
                    <div class="inputLabel short" title="Selects a 302 redirect">302</div>
                    <a class="removeVanity" href="#" title="Remove this vanity configuration"><span>Remove</span></a>
                </div>
            </div>
        </div>  
    </div>  
</div>


<script>
    // *************************************************************************
    // Sync jsp variables with js
    var rom = rom || {};
    rom.defaultVanityText = "<%=vanityDefaultText %>";
    rom.defaultTargetText = "<%=targetDefaultText %>";
    rom.defaultRedirectType = "<%=defaultRedirectType %>";
    rom.errorClass = "<%=errorClass %>";
    rom.NEW_VANITY = "<%=newVanity %>";
    rom.REDIRECT_TYPE_JSON_KEY = "<%=ROMConstants.REDIRECT_TYPE_JSON_KEY%>";
    rom.VANITY_URI_JSON_KEY = "<%=ROMConstants.VANITY_URI_JSON_KEY%>";
    rom.TARGET_JSON_KEY = "<%=ROMConstants.TARGET_JSON_KEY%>";
    rom.NODE_PATH_JSON_KEY = "<%=ROMConstants.NODE_PATH_JSON_KEY%>";
    rom.NODE_ATTR = "<%=NODE_ATTR %>";
    rom.CURRENT_RESOURCE = "<%=resource.getPath()%>";
    rom.CONFIG_PATH = "<%=configPath%>";
    rom.NEW_VANITY = "<%=newVanity %>";
    rom.DELETE_VANITY_REQUEST_TYPE = "<%=ROMConstants.DELETE_VANITY_REQUEST_TYPE %>";
    rom.UPDATE_VANITY_REQUEST_TYPE = "<%=ROMConstants.UPDATE_VANITY_REQUEST_TYPE %>";
    rom.sortParam = "<%=sortParam %>";
    rom.sortDescendClass = "<%=sortDescend %>";
 // *************************************************************************
</script>



