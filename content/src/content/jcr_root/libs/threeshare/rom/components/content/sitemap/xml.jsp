<%@ page import="org.apache.commons.lang.StringEscapeUtils,
                     org.apache.jackrabbit.util.Text,
                     com.threeshare.rom.search.SearchUtil,
                     com.day.cq.wcm.foundation.Sitemap,
                     com.day.cq.wcm.foundation.Sitemap.Link,
                     com.day.cq.wcm.foundation.TextFormat,
                     com.day.cq.wcm.api.PageFilter,
                     com.day.cq.wcm.api.PageManager,
                     com.day.cq.wcm.api.WCMMode,
                     java.io.IOException,
                     java.io.Writer,
                     java.io.PrintWriter,
                     java.util.Iterator,
                     java.util.Map,
                     java.util.LinkedList,
                     java.util.List,
                     java.util.ArrayList,
                     java.util.Arrays,
                     java.util.Calendar,
                     java.text.SimpleDateFormat,
                     java.util.Date"%><%
%><%@include file="/libs/foundation/global.jsp"%><%

    response.setContentType("application/xml");
    

    //##########################################################################
    //Process Query Parameters
    //##########################################################################
    String defaultPath = "/content/geometrixx";
    String rootPath = request.getParameter("path");
    if (rootPath == null || rootPath.length() == 0)
    {
        rootPath = defaultPath;
    }
    
    String defaultSite = "";
    String sitePath = request.getParameter("site");
    if (sitePath == null || sitePath.length() == 0)
    {
        sitePath = defaultSite;
    }
    
    String defaultRemove = "";
    String removePath = request.getParameter("remove");
    if (removePath == null || removePath.length() == 0)
    {
        removePath = defaultRemove;
    }

    String defaultChangeFreq = "weekly";
    String changeFreq = request.getParameter("frequency");
    if (changeFreq == null || changeFreq.length() == 0)
    {
        changeFreq = defaultChangeFreq;
    }
    
    String defaultExtension = ".html";
    String extension = request.getParameter("extension");
    if (extension == null || extension.length() == 0)
    {
        extension = defaultExtension;
    }
    
    String defaultLevelStr = "100";
    String levelStr = request.getParameter("level");
    if (levelStr == null || levelStr.length() == 0)
    {
        levelStr = defaultLevelStr;
    }
    
    int level = Integer.parseInt(defaultLevelStr);
    try 
    {
        if (Integer.parseInt(levelStr) == (int)Integer.parseInt(levelStr))
        {
            level = Integer.parseInt(levelStr);
        }   
    }
    catch(NumberFormatException nFE) 
    {
        //Not an integer, using default value
    }
    
    String defaultLinkPriority = "0.5";
    
    String[] additionalSearch = null;
    String additionalSearchParam = request.getParameter("add");
    if (additionalSearchParam != null && additionalSearchParam.length() != 0)
    {
        additionalSearch = additionalSearchParam.split(",");
    }
    
    //Add Exclusions
    String defaultExcludePaths = "";
    String ExcludePathsParam = request.getParameter("exclude");
    if (ExcludePathsParam == null || ExcludePathsParam.length() == 0)
    {
    ExcludePathsParam = defaultExcludePaths;
    }
    List<String> exclusions = new ArrayList<String>(Arrays.asList(ExcludePathsParam.split(",")));
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
    PageManager pm = slingRequest.getResourceResolver().adaptTo(PageManager.class);
    Page rootPage = pm.getPage(rootPath);

    %><?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><%
    
    //##########################################################################
    //Create Sitemap
    //##########################################################################
    if (rootPage != null)
    {
        Sitemap stm = new Sitemap(rootPage);
        LinkedList<Sitemap.Link> sitemapList = stm.getLinks();
        
        for (Sitemap.Link link : sitemapList)
        {
            if (link.getLevel() <= level)
            {   
                String lastMod = "";
                String linkPath = link.getPath();
                Page linkPage = pm.getPage(linkPath);
    
                // handles link priority
                String linkPriority = "0.5";
                int currentLevel = link.getLevel();
                if (currentLevel == 0 )
                {
                   linkPriority = "1.0";
                } 
                else if (currentLevel == 1)
                {
                    linkPriority = "0.8";
                }
    
                if (linkPage != null)
                {
                    Calendar lastModCal = linkPage.getLastModified();
                    if (lastModCal != null)
                    {
                        Date lastModDate = lastModCal.getTime();
                        lastMod = dateFormat.format(lastModDate);
                    }
                }
                
                // Exclusions                           
                boolean ignore = false;
                for (String exclusion: exclusions)
                {
                    if (linkPath.contains(exclusion) && exclusion.length() !=0)
                    {
                        ignore = true;
                    }
                }
  
                if (!ignore)
                {
                    String cleanedLinkPath = linkPath.replace(removePath,"");
                    String renderExtension = extension;
                    if (cleanedLinkPath.equals("/") || cleanedLinkPath.length() == 0)
                    {
                        renderExtension = "";
                    }
                    
                    //Remove portion of path defined by 'remove' parameter 
                    //and add value of 'site' parameter
                    linkPath = sitePath + cleanedLinkPath;
                %>
        <url>
            <loc>
                <%=linkPath%><%=renderExtension %>
            </loc>
            
            <% if (lastMod != null && lastMod.length() > 0){%>
            
            <lastmod> 
                <%=lastMod%>
            </lastmod>
            
            <%}%>
            
            <changefreq>
                <%=changeFreq %>
            </changefreq>
            
            <priority>
                <%=linkPriority%>
            </priority>
        </url><%
                }
            }
        }
        
        //Add any additonal search paths
        if (additionalSearch != null && additionalSearch.length != 0)
        {
            for (String searchPath: additionalSearch)
            {
                if (searchPath != null && searchPath.trim().length() != 0)
                {
                    if (!searchPath.startsWith("/"))
                    {   
                        searchPath = "/" + searchPath;
                    }
                    List<String> additionalPaths = SearchUtil.getChildPages(searchPath, resourceResolver);
                    for (String pagePath: additionalPaths)
                    {
                        String lastMod = "";
                        String linkPath = pagePath;
                        Page linkPage = pm.getPage(pagePath);
                        
                        if (linkPage != null)
                        {
                            Calendar lastModCal = linkPage.getLastModified();
                            if (lastModCal != null)
                            {
                                Date lastModDate = lastModCal.getTime();
                                lastMod = dateFormat.format(lastModDate);
                            }
                        }
                        
                        //Exclusions
                        boolean ignore = false;
                        for (String exclusion: exclusions)
                        {
                            if (linkPath.contains(exclusion))
                            {
                                ignore = true;
                            }
                        }
                       
                        if (!ignore)
                        {
                            String cleanedLinkPath = linkPath.replace(removePath,"");
                            String renderExtension = extension;
                            if (cleanedLinkPath.equals("/") || cleanedLinkPath.length() == 0)
                            {
                                renderExtension = "";
                            }
                            
                            //Remove portion of path defined by 'remove' parameter 
                            //and add value of 'site' parameter
                            linkPath = sitePath + cleanedLinkPath;
                        %>
    <url>
        <loc>
            <%=linkPath%><%=renderExtension %>
        </loc>
        <% if (lastMod != null && lastMod.length() > 0){%>
        
        <lastmod> 
            <%=lastMod%>
        </lastmod>
        
        <%}%>
        <changefreq>
            <%=changeFreq %>
        </changefreq>
        <priority>
            <%=defaultLinkPriority%>
        </priority>
    </url><%
                        }
                    }
                
                }
            }
        }
    }
%>
</urlset>