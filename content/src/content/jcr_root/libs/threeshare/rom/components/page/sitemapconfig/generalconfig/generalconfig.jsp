<%@include file="/libs/foundation/global.jsp" %>
<%

String siteDomain = properties.get("siteDomain","");
String pageExtension = properties.get("pageExtension","");
String[] searchPaths = properties.get("searchPaths",String[].class);
String[] damSearchPaths = properties.get("damSearchPaths",String[].class);
String[] excludePaths = properties.get("excludePaths",String[].class);
String[] excludeTags = properties.get("excludeTags",String[].class);

%>
<h3>General Config</h3>

<table>
	<tbody>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Site Domain</p></td>
		<td class="specRight layoutBorder"><p><%=siteDomain%></p></td>
	</tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Root Page</p></td>
		<td class="specRight layoutBorder"><p>${properties.rootPagePath}</p></td>
	</tr>    

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Additional Search Paths</p></td>
		<td class="specRight layoutBorder">
        <%
            if (searchPaths != null)
            {
                for(String searchPath : searchPaths)
                {
                    %><p><%=searchPath%></p><%
                }
            }
		%>
        </td>
    </tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>DAM Search Paths</p></td>
		<td class="specRight layoutBorder">
        <%
            if (damSearchPaths != null)
            {
                for(String damSearchPath : damSearchPaths)
                {
                    %><p><%=damSearchPath%></p><%
                }
            }
		%>
        </td>
    </tr>

	<tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Page Extension</p></td>
		<td class="specRight layoutBorder"><p><%=pageExtension%></p></td>
	</tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Change Frequency</p></td>
		<td class="specRight layoutBorder"><p>${properties.changeFrequency}</p></td>
	</tr>

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Max Depth</p></td>
		<td class="specRight layoutBorder"><p>${properties.maxLevels}</p></td>
	</tr>    

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Excluded Paths</p></td>
		<td class="specRight layoutBorder">
        <%
            if (excludePaths != null)
            {
                for(String excludePath : excludePaths)
                {
                    %><p><%=excludePath%></p><%
                }
            }
		%>
        </td>
    </tr>    

    <tr class="twoColumnSpec">
		<td class="specLeft layoutBorder"><p>Excluded Tags</p></td>
		<td class="specRight layoutBorder">
        <%
            if (excludeTags != null)
            {
                for(String excludeTag : excludeTags)
                {
                    %><p><%=excludeTag%></p><%
                }
            }
		%>
        </td>
    </tr>    

	</tbody>
</table>

<div class="Preview">
	<br>
	<br>
	<a href="<%= currentPage.getPath() %>.xml" target="blank">Preview Sitemap</a>
	<br>
	<div>
		<p>Please Note that the preview displayed in the author will show the full path as opposed the shortened path that will be displayed once viewed on the publisher. </p>
	</div>
	
</div>
