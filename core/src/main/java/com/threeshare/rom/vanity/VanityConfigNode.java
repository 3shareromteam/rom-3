/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/

package com.threeshare.rom.vanity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.threeshare.rom.util.AbstractNodeWrapper;
import com.threeshare.rom.util.NodeUtils;
import com.threeshare.rom.util.ROMConstants;

public class VanityConfigNode extends AbstractNodeWrapper
{
	private static final Logger log = LoggerFactory.getLogger(VanityConfigNode.class);
	
	public VanityConfigNode(ResourceResolver resolver, Node node) {
		super(resolver, node);
		
		try
		{
			if (node == null)
			{
				throw new IllegalArgumentException( "Invalid Config Node");
			}
			
			boolean validConfigNode = false;
			if (node.hasProperty(ROMConstants.NODE_TYPE_PROPERTY))
			{
				if(node.getProperty(ROMConstants.NODE_TYPE_PROPERTY)
						.getString().equals(ROMConstants.VANITY_CONFIG_NODE_TYPE))
				{
					validConfigNode = true;
				}
			}
			
			if (!validConfigNode)
			{
				throw new IllegalArgumentException( "Invalid Config Node");
			}

		}
		catch (RepositoryException ex)
		{
			log.error(ex.toString(), ex);
			return;
		}
	}

	public void setVanityDomain(String vanityDomainPath)
    {
		// Courtesy method to allow single string 
		String[] vanityDomainArray = {vanityDomainPath};
		setVanityDomain(vanityDomainArray);
		return;
    }
	
	public void setVanityDomain(String[] vanityDomainPaths)
    {
		// Exclusions allow certain paths to be ignored
		try
        {
            Node node = getNode();
            NodeUtils.setArrayProperty(node, ROMConstants.CONFIG_NODE_VANITY_DOMAIN_PROPERTY, vanityDomainPaths);
            node.getSession().save();
            
        }
        catch (Exception ex)
        {
        	log.error("Error occured while setting exclusions", ex);
        }
        
        return;
    }
	
	public String[] getVanityDomain()
	{
		String[] vanityDomains = null;
		String defaultVanityDomain = "/content";
		try
		{
			Node contentNode = getNode();
			if (contentNode.hasProperty(ROMConstants.CONFIG_NODE_VANITY_DOMAIN_PROPERTY))
			{
				vanityDomains = NodeUtils.getArrayProperty(contentNode, ROMConstants.CONFIG_NODE_VANITY_DOMAIN_PROPERTY);
			}
			
			if (vanityDomains == null)
			{
				vanityDomains = new String[]{defaultVanityDomain};
			}
			
			
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return vanityDomains;
	}
	
	public void setExclusions(String exclusion)
    {
		// Courtesy method to allow single string 
		String[] exclusionArray = {exclusion};
		setExclusions(exclusionArray);
		return;
    }
	
	public void setExclusions(String[] exclusions)
    {
		// Exclusions allow certain paths to be ignored
		try
        {
            Node node = getNode();
            NodeUtils.setArrayProperty(node, ROMConstants.CONFIG_NODE_EXCLUSIONS_PROPERTY, exclusions);
            node.getSession().save();
            
        }
        catch (Exception ex)
        {
        	log.error("Error occured while setting exclusions", ex);
        }
        
        return;
    }
	
	public String[] getExclusions()
	{
		String[] exclusions = null;
		
		try
		{
			Node contentNode = getNode();
			if (contentNode.hasProperty(ROMConstants.CONFIG_NODE_EXCLUSIONS_PROPERTY))
			{
				exclusions = NodeUtils.getArrayProperty(contentNode, ROMConstants.CONFIG_NODE_EXCLUSIONS_PROPERTY);
			}
			
			if (exclusions == null)
			{
				exclusions = new String[]{};
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return exclusions;
	}
	
	public void setVanityType(String vanityType)
    {
        try
        {
        	String type = ROMConstants.VANITY_TYPE_STANDARD;
        	if (vanityType.equals(ROMConstants.VANITY_TYPE_CUSTOM))
        	{
        		type = ROMConstants.VANITY_TYPE_CUSTOM;
        	}
        	
            Node vanityContentNode = getNode();
            vanityContentNode.setProperty(
            		ROMConstants.VANITY_TYPE_PROPERTY, type);
            vanityContentNode.getSession().save();
            
        }
        catch (Exception ex)
        {
            log.error(ex.toString(), ex);
        }
    }
	
	public String getVanityType()
	{
		// Default to OOB vanity type
		String contentPath = ROMConstants.VANITY_TYPE_STANDARD;
		try
		{
			Node contentNode = getNode();
			if (contentNode.hasProperty(ROMConstants.VANITY_TYPE_PROPERTY))
			{
				contentPath = contentNode.getProperty(ROMConstants.VANITY_TYPE_PROPERTY).getString();
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

		}
		return contentPath;
	}
	
	public VanityNode createVanity(String vanityURI, String vanityTarget)
	{
		return createVanity(vanityURI, vanityTarget, ROMConstants.DEFAULT_REDIRECT_TYPE);
	}
	
	public VanityNode createVanity(String vanityURI, String vanityTarget, String redirectType)
	{
		VanityNode vanity = null;
		
		Session session = resolver.adaptTo(Session.class);

		String userId = session.getUserID();
		Node vanityNode = null;

		try
		{
			//generate random number for vanity name
			long random = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
			String nodeName = ROMConstants.VANITY_NODE_PREFIX + random;
			
			Node configNode = getNode();
			
			// Create hierarchy to handle 1000+ nodes 
			String randomStr = String.valueOf(random);
			String level1 = randomStr.substring(0, 2);
			String level2 = randomStr.substring(2, 4);
			String level3 = randomStr.substring(4, 6);
			
			Node node1 = NodeUtils.createNode(resolver, configNode, level1, false);
			Node node2 = NodeUtils.createNode(resolver, node1, level2, false);
			Node node3 = NodeUtils.createNode(resolver, node2, level3, false);
			
			// Use configNode root if node creation fails.
			if (node3 == null)
			{
				node3 = configNode;
			}
			
			vanityNode = NodeUtils.createNode(resolver, node3, nodeName, true);

			Node vanityContentNode = vanityNode.getNode(JcrConstants.JCR_CONTENT);

			vanityContentNode.setProperty(ROMConstants.NODE_TYPE_PROPERTY,
				ROMConstants.VANITY_NODE_TYPE);

			vanity = VanityFactory.getVanity(resolver, vanityNode);

			// Set Vanity properties
			vanity.setVanityTarget(vanityTarget);
			vanity.setVanityURI(vanityURI);
			vanity.setRedirectType(redirectType);
			
			session.save();

			log.info("[{}] Vanity created: {}", userId, vanity.getPath());

		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);

			if (vanityNode != null)
			{
				try
				{
					vanityNode.remove();

					session.save();
				}
				catch (Exception ex2)
				{
					log.error(ex2.toString(), ex2);
				}
			}
		}

		return vanity;
	}
	
	public List<VanityNode> getVanities ()
	{
		String searchPath = getPath();
		return getVanities(searchPath);
	}
	
	public List<VanityNode> getVanities (String searchPath)
	{
		List<VanityNode> vanities = new ArrayList<VanityNode>();
		
		try
		{
		
			QueryBuilder queryBuilder = resolver.adaptTo(QueryBuilder.class);
			Session session = resolver.adaptTo(Session.class);
	
			Map<String, String> map = new HashMap<String, String>();
			map.put("path", searchPath);
			map.put("p.limit", "100000");
			map.put("nodename", ROMConstants.VANITY_NODE_PREFIX + "*");
			
			Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
			SearchResult result = query.getResult();
	
			for (Hit hit : result.getHits())
		    {
		        Resource hResource = hit.getResource();
		        if (hResource != null)
		        {
		        	VanityNode vanity = VanityFactory.getVanity(resolver, hResource);
			        vanities.add(vanity);
		        }
		    }
		}
		catch(Exception ex)
		{
			log.error("SearchUtil Error for search path:" + searchPath,  ex);
		}
		
		return vanities;
	}
	
}
