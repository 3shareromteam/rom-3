/*
 * Copyright 1997-2008 Day Management AG
 * Barfuesserplatz 6, 4001 Basel, Switzerland
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Day Management AG, ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Day.
 */
package com.threeshare.rom.linkrewriter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import java.util.Iterator;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.util.RequestUtils;

import org.apache.jackrabbit.api.security.user.*;


@SuppressWarnings("serial")
@Component(metatype = true, immediate = true, label = "3|SHARE ROM Link Rewriter Servlet", description = "Servlet for link rewriting.")
@Service(value = Servlet.class)
@Properties({
	@Property (name="service.vendor", value="3|SHARE"),
	@Property (name="service.pid", value="com.threeshare.rom.linkrewriter.LinkRewriterServlet")
})

public class LinkRewriterServlet extends SlingAllMethodsServlet {

    private static Logger log = LoggerFactory.getLogger(LinkRewriterServlet.class);
    
	private static String[] authorizedUsers;
	private static String[] authorizedGroups;
		
	@Property (label = "Link Rewriter Servlet Path", value = "/bin/threeshare/linkrewriter")
	protected static final String SLING_SERVLET_PATHS = "sling.servlet.paths";
	
	@Property (label = "Authorized Users", description = "Users allowed to access link rewrite tools", value = { "admin" , ""})
	protected static final String AUTHORIZED_USERS_CONF = "authorizedUsers.value.config";

	@Property (label = "Authorized Groups", description = "Groups allowed to access link rewrite tools", value = { "" })
	protected static final String AUTHORIZED_GROUPS_CONF = "authorizedGroups.value.config";

	@SuppressWarnings("unchecked")
	@Activate
    protected void activate(ComponentContext context) throws Exception
    {
		// On activation, retrieve values from OSGI configuration.
		final Dictionary<String, Object> properties = context.getProperties();
		authorizedUsers = PropertiesUtil.toStringArray(properties.get(AUTHORIZED_USERS_CONF));
		authorizedGroups = PropertiesUtil.toStringArray(properties.get(AUTHORIZED_GROUPS_CONF));
    }
	
    @Override
    protected void doGet(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws ServletException,
            IOException {

    	handleRequest(request, response);
    }

    @Override
    protected void doPost(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws ServletException,
            IOException {
        
        handleRequest(request, response);
    }
    
    private void handleRequest(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws IOException,
            ServletException {
    	
    	boolean isAuthorized = isAuthorized(request.getResourceResolver());
    	if (isAuthorized)
    	{
    		processRequest(request, response);
    	}
    	else
    	{
    		// Not Authorized
    		String message = "These aren't the droids you're looking for";
    		int code = 404;
    		writeHtmlResponse (response, message, code);
    	}
    }

    private void processRequest(SlingHttpServletRequest request,
            SlingHttpServletResponse response) throws IOException,
            ServletException {
        
        response.setContentType("text/html;charset=UTF-8");
        Session session = request.getResourceResolver().adaptTo(Session.class);
        String parentNode= RequestUtils.getParameter(request,"rootNode");
        String originalPath = RequestUtils.getParameter(request,"originalPath");
        String newPath = RequestUtils.getParameter(request,"newPath");
        
        try 
        {
        	if (parentNode == null || parentNode.length() < 1)
        	{
        		log.error("LinkRewriter Error: Parent Node not defined");
        		return;
        	}
        	if (originalPath == null || originalPath.length() < 1)
        	{
        		log.error("LinkRewriter Error: Original Path not defined");
        		return;
        	}
        	if (newPath == null || newPath.length() < 1)
        	{
        		log.error("LinkRewriter Error: New Path not defined");
        		return;
        	}
        	Resource rootResource = request.getResourceResolver().getResource(parentNode);
        	if (rootResource == null)
        	{
        		log.error("LinkRewriter Error: Parent Node does not exist");
        		return;
        	}
        	
        	rewriteLinks(rootResource, originalPath, newPath, session);
        	
        	PrintWriter out = response.getWriter();
        	
        	StringBuilder sb = new StringBuilder();
        	sb.append("Link Rewriter Tool");
            sb.append("<br>");
            sb.append("Root Resource: " + parentNode);
            sb.append("<br>");
            sb.append("Original Path: " +  originalPath);
            sb.append("<br>");
            sb.append("New Path: " + newPath);
            sb.append("<br>");
	        out.println(sb.toString());

        } 
        catch (Exception e)
        {
        	log.error("LinkRewriter Error:", e);
        }
        finally
        {
        	
        }
        
    }

	private boolean isAuthorized(ResourceResolver resourceResolver) 
	{
		boolean isAuthorized = false;
		Authorizable currentUser = resourceResolver.adaptTo(User.class);
		String currentUserID = "";
		try 
		{
			currentUserID = currentUser.getID();
			Iterator<Group> currentUserGroups = currentUser.memberOf();
			
			for (String authorizedUser : authorizedUsers)
			{
				if (authorizedUser.equals(currentUserID))
				{
					isAuthorized = true;
					break;
				}
			}
			
			while(currentUserGroups.hasNext() && !isAuthorized) 
		    {
				Group group = currentUserGroups.next();
				String groupID = group.getID();
				
				for (String authorizedGroup : authorizedGroups)
				{
					if (authorizedGroup.length() > 0 && authorizedGroup.equals(groupID))
					{
						isAuthorized = true;
						break;
					}
				}
					        
		    }
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		log.info("LinkRewriter called by " + currentUserID);
		return isAuthorized;
	}
   
    private void rewriteLinks(Resource resource, String originalPath, String newPath, Session session){

    	if(resource != null){
    		LinkRewriter.rewrite(resource, originalPath, newPath, session);

    		Iterator<Resource> riterator = resource.getChildren().iterator();

    		while(riterator.hasNext()){
    			Resource next = riterator.next();
    			rewriteLinks(next, originalPath, newPath, session);
    		}
    	}

    }
    
	private static void writeHtmlResponse(SlingHttpServletResponse response, String msg, int code) throws IOException
	{
		String contentType = "text/html;charset=UTF-8";
		writeResponse(response, msg, code, contentType);
	}
	
	private static void writeResponse(SlingHttpServletResponse response, String msg, int code, String contentType)  throws IOException 
	{
		response.setStatus(code);
		response.setContentType(contentType);
		PrintWriter out = response.getWriter();
		out.println(msg);
		out.close();		
	}
}
