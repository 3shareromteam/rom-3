package com.threeshare.rom.audit;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype = true, immediate = true, label = "3|SHARE - Audit - Resource Listener", description = "Resource Listener: Logs changes to resources in specified paths.")
@Service(value = EventHandler.class)

public class ResourceListener implements EventHandler {

	@Property (label = "Event Topic", description = "Audited Event Topic", value = "org/apache/sling/api/resource/Resource/*", propertyPrivate = true )
	protected static final String EVENT_TOPIC = EventConstants.EVENT_TOPIC;

	@Property (label = "Audited Paths", description = "Define the paths to be audited.", value = { "/content" , "/etc" , "/apps" , "/home" })
	protected static final String AUDIT_PATHS = "auditedPaths.value.config";

	@Property (label = "Auditing Enabled", description = "Option to enable/disable auditing. ", boolValue = true)
	protected static final String AUDIT_ENABLED = "audit.config.enabled";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static String[] auditPaths;
    private static boolean auditingEnabled = true;
    private static String eventPrefix = "org/apache/sling/api/resource/Resource/";

    public void  handleEvent(final Event event) {
    	if (auditingEnabled)
		{
    		String eventPath = event.getProperty(SlingConstants.PROPERTY_PATH).toString();
    		String eventUser = event.getProperty(SlingConstants.PROPERTY_USERID).toString();
    		String eventTopic = event.getTopic().replace(eventPrefix, "");

    		boolean isValidPath = validatePath(auditPaths,eventPath);

    		if (isValidPath) {
    			logger.debug("Resource Event: " + eventUser + " " + eventTopic + " " + eventPath);
    		}
		}
    }

    private boolean validatePath (String[] auditPaths, String resourcePath) {
    	boolean valid = false;

    	if (resourcePath != null && resourcePath.length() > 0) {
    		for (String auditPath : auditPaths) {
    			if (auditPath != null && auditPath.length() > 0) {
	    			if (resourcePath.startsWith(auditPath)) {
	    				valid = true;
	    			}
    			}
    		}
    	}

    	return valid;
    }

    @SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) {
    	// On activation, retrieve values from OSGI configuration.
    	final Dictionary<String, Object> properties = context.getProperties();
    	auditPaths = PropertiesUtil.toStringArray(properties.get(AUDIT_PATHS));
    	auditingEnabled = PropertiesUtil.toBoolean(properties.get(AUDIT_ENABLED),true);
    }

    @Deactivate
    protected void deactivate() {
    	auditPaths = null;
    }
}
