package com.threeshare.rom.backup.impl;

import com.threeshare.rom.backup.ROMBackup;
import com.threeshare.rom.backup.data.BackupData;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.felix.scr.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Service
@Properties({
    @Property(name = "service.description", value = "ROM Backup Data Collector"),
    @Property(name = "service.vendor", value = "3|SHARE")
})
public class ROMBackupImpl implements ROMBackup {

    @Reference
    private MBeanServer mbeanServer;
    private final Logger log = LoggerFactory.getLogger(getClass());

    public String getBackupData() throws Exception {
        try {
            Boolean backupWasSuccessful = (Boolean) mbeanServer.getAttribute(new ObjectName("com.adobe.granite:type=Repository"), "BackupWasSuccessful");
            Boolean backupInProgress = (Boolean) mbeanServer.getAttribute(new ObjectName("com.adobe.granite:type=Repository"), "BackupInProgress");
            String backupResult = (String) mbeanServer.getAttribute(new ObjectName("com.adobe.granite:type=Repository"), "BackupResult");
            Integer backupProgress = (Integer) mbeanServer.getAttribute(new ObjectName("com.adobe.granite:type=Repository"), "BackupProgress");
            String backupTarget = (String) mbeanServer.getAttribute(new ObjectName("com.adobe.granite:type=Repository"), "CurrentBackupTarget");
            
            BackupData backupData = new BackupData();
            backupData.setBackupInProgress(backupInProgress);
            backupData.setBackupTarget(backupTarget);
            backupData.setBackupProgress(backupProgress);
            backupData.setBackupResult(backupResult);
            backupData.setBackupWasSuccessful(backupWasSuccessful);
            return backupData.toXML();
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }
}
