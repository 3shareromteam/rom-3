package com.threeshare.rom.monitor.revisioncleanup.data;

public class RevisionCleanupMonitorData {
	private String revisionCleanupStatus;
	private static final String PRTG_ERROR_TEXT = "Online Revision Cleanup failed";

	public void setBackupTarget(String revisionCleanupStatus) {
        this.revisionCleanupStatus = revisionCleanupStatus;
	}
	
	public String revisionCleanupStatus() {
        return revisionCleanupStatus;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		
		sb.append("<result>");
		sb.append("<channel>Revision Cleanup Status</channel>");
		sb.append("<Unit>Custom</Unit>");
		sb.append("<CustomUnit>Status</CustomUnit>");
        sb.append("<value>" + this.revisionCleanupStatus + "</value>");
		sb.append("</result>");
		
		if (!this.revisionCleanupStatus.equals("OK"))
		{
			sb.append("<Text>");
			sb.append(PRTG_ERROR_TEXT);
			sb.append("</Text>");
			
			sb.append("<Error>");
			sb.append(this.revisionCleanupStatus);
			sb.append("</Error>");
		}
		
		sb.append("</prtg>");
		return sb.toString();
	}
	
}
