/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.bundles;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BundleMonitor
{
	private static final Logger log = LoggerFactory.getLogger(BundleMonitor.class);
	private static final String BUNDLE_NAME_KEY = "Bundle Name";
	private static final String BUNDLE_VERSION_KEY = "Bundle Version";
	private static final String BUNDLE_STATE_KEY = "Bundle State";
	private static final String BUNDLE_ID_KEY = "Bundle ID";
	private static final String PRTG_ERROR_TEXT = "A bundle has been detected in not running state.";
	private static final String PRTG_SUCCESS_TEXT = "All monitored bundles are active.";
		
	
	protected static String processXmlBundleRequest(Bundle[] bundles)
	{
		// Create custom xml for PRTG
		int prtgStatus = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		for (Bundle bundle : bundles)
		{
			
			sb.append("<result>");
			
			sb.append("<channel>");
			sb.append(bundle.getSymbolicName());
			sb.append("</channel>");
			
			sb.append("<value>");
			sb.append(bundle.getState());
			sb.append("</value>");
		
			sb.append("</result>");
			
			prtgStatus = prtgStatus + getBundleStatePrtgStatus(bundle.getState());
		
		}
		
		String message = PRTG_SUCCESS_TEXT;
		String sensorStatus = "0";
		
		if (prtgStatus > 0)
		{
			message = PRTG_ERROR_TEXT;
			sensorStatus = "1";
		}
		
		sb.append("<Text>");
		sb.append(message);
		sb.append("</Text>");
		
		sb.append("<Error>");
		sb.append(sensorStatus);
		sb.append("</Error>");
		
		sb.append("</prtg>");
		
		return sb.toString();
	}
	
	protected static String processHtmlBundleRequest(Bundle[] bundles)
	{
		StringBuilder sb = new StringBuilder();
		for (Bundle bundle : bundles)
		{
			try 
			{
				sb.append("Bundle Name: " + bundle.getSymbolicName());
				sb.append("<br>");
				sb.append("Bundle Version: " + bundle.getVersion());
				sb.append("<br>");
				sb.append("Bundle ID: " + bundle.getBundleId());
				sb.append("<br>");
				sb.append("Bundle Status: " + BundleUtils.getBundleState (bundle));
				sb.append("<br>");
				sb.append("<br>");
			}
			catch (Exception e) 
			{
				log.error ("Exception in BundleMonitor", e);
			}
		}
		
		return sb.toString();
	}
	
	protected static String processJsonBundleRequest(Bundle[] bundles)
	{
		JSONObject jsonObject = processJSONBundleRequest(bundles);
		
		return jsonObject.toString();
	}
	
	private static JSONObject processJSONBundleRequest(Bundle[] bundles)
	{
		JSONObject jsonObject = new JSONObject();
		for (Bundle bundle : bundles)
		{
			JSONObject bundleJsonObject = new JSONObject();
			
			try 
			{
				// Get Bundle Name, Version, State
				String bundleName = bundle.getSymbolicName();
				bundleJsonObject.put(BUNDLE_NAME_KEY, bundleName);
				
				bundleJsonObject.put(BUNDLE_VERSION_KEY, bundle.getVersion());
				
				String bundleState = BundleUtils.getBundleState (bundle);
				bundleJsonObject.put(BUNDLE_STATE_KEY, bundleState);
				
				bundleJsonObject.put(BUNDLE_ID_KEY, bundle.getBundleId());
				
				jsonObject.put(bundleName, bundleJsonObject);
				
			}
			catch (JSONException e) 
			{
				log.error ("Json Exception in BundleMonitor", e);
			}
		}
		
		return jsonObject;
	}
		
	private static int getBundleStatePrtgStatus (int bundleStateCode)
	{
		// Return 0 for valid bundle status
		int status = 1; 
		
		if (bundleStateCode == Bundle.ACTIVE)
		{
			status = 0;
		}
		else if (bundleStateCode == Bundle.INSTALLED)
		{
			status = 1;
		}
		else if (bundleStateCode == Bundle.RESOLVED)
		{
			status = 1;
		}
		else if (bundleStateCode == Bundle.STARTING)
		{
			status = 1;
		}
		else if (bundleStateCode == Bundle.UNINSTALLED)
		{
			status = 1;
		}
		else if (bundleStateCode == Bundle.STOPPING)
		{
			status = 1;
		}
		
		return status;
	}
	
}