/*************************************************************************

  Copyright 2013 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

**************************************************************************/
package com.threeshare.rom.bundles;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.Activator;

public class BundleUtils {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(BundleUtils.class);
	
	public static Bundle[] getBundles()
	{
		Bundle[] bundles = Activator.getInstance().getBundles();
		return bundles;
	}
	
	public static String[] getBundleNames ()
	{
		Bundle[] bundles = getBundles();
		return getBundleNames(bundles);
	}
	
	public static String[] getBundleNames (Bundle[] bundles)
	{
		List<String> bundleNamesList = new ArrayList<String>();
		
		for (Bundle bundle : bundles)
		{
			String bundleName = bundle.getSymbolicName();
			bundleNamesList.add(bundleName);
		}
		
		String[] bundleNamesArray = bundleNamesList.toArray(new String [bundleNamesList.size()]); 
		
		return bundleNamesArray;
	}

	public static Bundle[] getFilteredBundles(String filter)
	{
		String[] filters = {filter};
		return getFilteredBundles(filters);
	}
	
	public static Bundle[] getFilteredBundles(String[] filters)
	{
		Bundle[] bundles = getBundles();
		List<Bundle> bundleList = new ArrayList<Bundle>();
		
		if (filters != null)
		{
			for (Bundle bundle : bundles)
			{
				String bundleName = bundle.getSymbolicName();
				for (String filter : filters)
				{
					if (bundleName.contains(filter))
					{
						bundleList.add(bundle);
					}
				}
			}
		}
		else
		{
			// For invalid filters, return full list.
			return bundles;
		}
		
		Bundle[] filteredBundleArray = bundleList.toArray(new Bundle [bundleList.size()]);
		
		return filteredBundleArray;
	}
	
	public static String getBundleState (Bundle bundle)
	{
		int bundleStateCode = bundle.getState();
		return getBundleState (bundleStateCode);
	}
	
	public static String getBundleState (int bundleStateCode)
	{
		String status = "unknown"; 
		
		if (bundleStateCode == Bundle.ACTIVE)
		{
			status = "Active";
		}
		else if (bundleStateCode == Bundle.INSTALLED)
		{
			status = "Installed";
		}
		else if (bundleStateCode == Bundle.RESOLVED)
		{
			status = "Resolved";
		}
		else if (bundleStateCode == Bundle.STARTING)
		{
			status = "Starting";
		}
		else if (bundleStateCode == Bundle.UNINSTALLED)
		{
			status = "Uninstalled";
		}
		else if (bundleStateCode == Bundle.STOPPING)
		{
			status = "Stopping";
		}
		
		return status;
	}
	
	
}
