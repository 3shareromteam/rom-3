package com.threeshare.rom.sync;

import java.util.Dictionary;

import javax.jcr.Node;
import javax.jcr.Session;

import com.day.cq.commons.jcr.JcrUtil;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype = true, immediate = true, label = "3|SHARE - Schema Sync", description = "Sync Listener: Syncs changes from one path to another path.")
@Service(value = EventHandler.class)

public class SyncListener implements EventHandler {
	
	@Property (label = "Event Topic", description = "Audited Event Topic", value = "org/apache/sling/api/resource/Resource/*", propertyPrivate = true )
	protected static final String EVENT_TOPIC = EventConstants.EVENT_TOPIC;
	
	@Property (label = "Source Path", description = "Define the sync path to be synchronzied. Target content path cannot be inside of the source path.", value = "/test/1")
	protected static final String SYNC_SOURCE_PATH = "syncSourcePath.value.config";
	
	@Property (label = "Target Path", description = "Define the target path to be synchronzied. Target content path cannot be inside of the source path.", value = "/test/2")
	protected static final String SYNC_TARGET_PATH = "syncTargetPath.value.config";
	
	@Property (label = "Sync Enabled", description = "Option to enable/disable auditing. ", boolValue = false)
	protected static final String SYNC_ENABLED = "sync.config.enabled";

	@Reference   
    private ResourceResolverFactory resourceResolverFactory;
	
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static String sourcePath = "";
    private static String targetPath = "";
    private static boolean syncEnabled = true;
    
    public void  handleEvent(final Event event) {
    	if (syncEnabled)
		{
    		ResourceResolver adminResolver = null;
    		
    		try
    		{
    			
    			adminResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
    			Session session = adminResolver.adaptTo(Session.class);

    			String eventPath = event.getProperty(SlingConstants.PROPERTY_PATH).toString();
    			if (eventPath != null && eventPath.length() > 0)
    			{
    				if (sourcePath != null && sourcePath.length() > 0 && targetPath != null && targetPath.length() > 0)
    				{
    					if (!targetPath.startsWith(sourcePath))
    					{
    						if (eventPath.startsWith(sourcePath))
    						{
    							log.info("Sync called for " + sourcePath + " to " + targetPath);
    							
    							Resource sourceResource = adminResolver.getResource(sourcePath);
    							Resource targetResource = adminResolver.getResource(targetPath);
    							
    							if (sourceResource != null && targetResource != null)
    							{
	    							Node sourceNode = (Node)sourceResource.adaptTo(Node.class);
	    							Node targetNode = (Node)targetResource.adaptTo(Node.class);
	    							
	    							if (sourceNode != null && targetNode != null)
		    			    		{
		    			    			Node dstParent = targetNode.getParent();
		    				    		
		    				    		JcrUtil.copy(sourceNode, dstParent, targetNode.getName());
		    				    		log.info("Copy completed from " + sourcePath + " to " + targetPath);
		    				    		session.save();
		    				    		session.logout();
		    			    		}
    							}
    						}
    					}
    					else
    					{
    						log.error("Target path can not be inside of source path.");
    					}
    				}
    				else
    				{
    					log.error("Empty source (" + sourcePath + ") or target (" + targetPath + ") path");
    				}
    			}
    			else
    			{
    				log.error("Empty event path");
    			}
		    }
        	catch (Exception e)
        	{
        		System.err.println("Exception: " + e.getMessage());
        	}
        	finally
        	{
        		if (adminResolver != null)
        		{
        			adminResolver.close();
        			adminResolver = null;
        		}
        	}
    	}	
	}

	@SuppressWarnings("unchecked")
    @Activate
    protected void activate(ComponentContext context) {
    	// On activation, retrieve values from OSGI configuration.
    	final Dictionary<String, Object> properties = context.getProperties();
    	sourcePath = PropertiesUtil.toString(properties.get(SYNC_SOURCE_PATH), "");
    	targetPath = PropertiesUtil.toString(properties.get(SYNC_TARGET_PATH), "");
    	syncEnabled = PropertiesUtil.toBoolean(properties.get(SYNC_ENABLED),true);
    }
    
    @Deactivate
    protected void deactivate() {
    }
}