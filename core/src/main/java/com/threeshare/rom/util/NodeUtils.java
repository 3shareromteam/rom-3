package com.threeshare.rom.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;

public class NodeUtils {
	
	private static final Logger log = LoggerFactory.getLogger(NodeUtils.class);
	
	public static String[] getArrayProperty(Node node, String property)
	{
		String[] values = null;

		try
		{
			if (node != null && node.hasProperty(property))
			{
				Property propertyObj = node.getProperty(property);
				if (propertyObj.isMultiple())
				{
					Value[] theValues = propertyObj.getValues();
					values = new String[theValues.length];
					for (int i = 0; i < theValues.length; i++)
					{
						values[i] = theValues[i].getString();
					}
				}
			}
		}
		catch (Exception ex)
		{
			log.error("Error retrieving Array property", ex);
		}

		return values;
	}
	
	public static void setArrayProperty(Node node, String property, String[] propertyValues)
	{
		try
		{
			node.setProperty(property, (propertyValues != null ? propertyValues : null));
		}
		catch (Exception ex)
		{
			log.error("Cannot set multi property", ex);
		}
	}
	
	public static Node createNode(ResourceResolver resolver, Node parentNode,
			String nodeName, boolean createJcrContentNode)
	{	
		return createNode(resolver, parentNode, nodeName, nodeName, createJcrContentNode);
	}
	
	public static Node createNode(ResourceResolver resolver, Node parentNode,
			String nodeName, String title, boolean createJcrContentNode)
	{
		// If no node type is specified, use nt:unstructured.
		String nodeType = JcrConstants.NT_UNSTRUCTURED;
		Node node = createNode(resolver, parentNode, nodeName, title, nodeType, createJcrContentNode);
		return node;
		
	}
	public static Node createNode(ResourceResolver resolver, Node parentNode,
			String nodeName, String title, String nodeType, boolean createJcrContentNode)
	{
		Session session = resolver.adaptTo(Session.class);
		
		Node node = null;
		try
		{

			if (parentNode.hasNode("./" + nodeName))
			{
				// Do nothing if node already exists.
				Node existingNode = parentNode.getNode(nodeName);
				return existingNode;
			}
			else
			{
				Calendar now = Calendar.getInstance();
				
				if (nodeName == null)
				{
					nodeName = Long.toString(now.getTimeInMillis());
				}
				
				nodeName = JcrUtil.createValidName(nodeName);
				
				if (nodeType == null || nodeType.length() == 0)
				{
					nodeType = JcrConstants.NT_UNSTRUCTURED;
				}
				
				node = JcrUtil.createUniqueNode(parentNode, nodeName,
						nodeType, session);
				
				if (createJcrContentNode)
				{
					node.setProperty(JcrConstants.JCR_CREATED, now);
					Node contentNode = node.addNode(JcrConstants.JCR_CONTENT,
						JcrConstants.NT_UNSTRUCTURED);
					contentNode.setProperty(JcrConstants.JCR_CREATED_BY, session.getUserID());
					contentNode.setProperty(JcrConstants.JCR_TITLE, title);
					contentNode.setProperty(JcrConstants.JCR_LASTMODIFIED, now);
					contentNode.setProperty(JcrConstants.JCR_LAST_MODIFIED_BY,
						session.getUserID());
				}
				
				session.save();
				
			}
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
			
			if (node != null)
			{
				try
				{
					node.remove();
					session.save();
				}
				catch (Exception ex2)
				{
					log.error(ex2.toString(), ex2);
				}
			}
		}
		return node;
	}

	public static List<String> crawlNodes (ResourceResolver resourceResolver, String rootNodePath)
	{
		// Set default maxNodes at 0
		int defaultMaxNodes = 10;		
		return crawlNodes (resourceResolver, rootNodePath, defaultMaxNodes);
	}
	
	public static List<String> crawlNodes (ResourceResolver resourceResolver, String rootNodePath, int maxNodes)
	{
		// Set default defaultMaxLevel at 1
		int defaultMaxLevel = 1;		
		return crawlNodes (resourceResolver, rootNodePath, maxNodes, defaultMaxLevel);
	}
	
	public static List<String> crawlNodes (ResourceResolver resourceResolver, String rootNodePath, int maxNodes, int maxLevels)
	{
		// Set default currentLevel at 0
		int defaultCurrentLevel = 0;	
		return crawlNodes (resourceResolver, rootNodePath, maxNodes, maxLevels, defaultCurrentLevel);
	}

	public static List<String> crawlNodes (ResourceResolver resourceResolver, String rootNodePath, int maxNodes, int maxLevel, int currentLevel)
	{
		return crawlNodes (resourceResolver, rootNodePath, maxNodes, maxLevel, currentLevel, null);
	}
	
	public static List<String> crawlNodes (ResourceResolver resourceResolver, String rootNodePath, int maxNodes, int maxLevel, int currentLevel, List<String> nodePaths)
	{
		if (nodePaths == null)
		{
			nodePaths = new ArrayList<String>();
		}
		
		if (currentLevel > maxLevel || nodePaths.size() >= maxNodes)
		{
			// current level cant be greater than max level
			// node path count can not be greater than max nodes count
			return nodePaths;
		}
			
		try
		{
			Resource rootResource = resourceResolver.getResource(rootNodePath);
			if (rootResource != null)
			{
				Node rootNode = rootResource.adaptTo(Node.class);
			    if (rootNode != null)
			    {
			    	if (currentLevel < maxLevel && rootNode.hasNodes())
			    	{
			    		NodeIterator childNodes = rootNode.getNodes();
			    		while (childNodes.hasNext() && nodePaths.size() < maxNodes && currentLevel < maxLevel)
			    		{
			    			Node childNode = childNodes.nextNode();
			    			int level = currentLevel + 1;
			    			nodePaths = crawlNodes (resourceResolver, childNode.getPath(), maxNodes, maxLevel, level, nodePaths);
			    		}
			    	}
			    	
			    	if (nodePaths.size() < maxNodes && currentLevel > 0)
    				{
	    				log.debug("NodeCrawl added " + rootNode.getPath() + " to the node list.");
    					nodePaths.add(rootNode.getPath());
    				}
			    	
			    }
			}
			
		}
		catch (Exception ex)
		{
			log.error(ex.toString(), ex);
		}
		finally
		{
			
		}
				
		return nodePaths;
	}
	
}